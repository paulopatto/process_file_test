$(function(){
  ///  http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/
  $('input.custom-upload-button').on('change', function(){
    input = $(this);
    nFiles = input.get(0).files ? input.get(0).files.length : 1;
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [nFiles, label]);
  });
  //$('input.custom-upload-button').on('fileselect', function(event, nFiles, label) { })

});
