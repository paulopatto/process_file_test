require 'csv'

class UploadController < ApplicationController
  HEADERS = %w(customer description price units address provider)

  def index
    @uploads = Upload.all
  end

  def upload
    log_file = upload_params.tempfile
    @upload = Upload.create

    data_from_file(log_file).each do |sale_attrs|
      @upload.sales.create(sale_attrs)
    end

    redirect_to root_path
  end

  protected
  def upload_params
    params.require(:sale_file)
  end

  def data_from_file(file)
    data = CSV.read(file, col_sep: "\t")
    data.delete_at(0)

    data.map do |row|
      row.each_with_index.map do |value, index|
        Hash[ HEADERS[index], value ]
      end.reduce(&:merge)
    end
  end
end
