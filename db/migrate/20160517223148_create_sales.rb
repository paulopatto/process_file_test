class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :customer
      t.string :description
      t.float :price
      t.integer :units
      t.string :address
      t.string :provider

      t.timestamps null: false
    end
  end
end
