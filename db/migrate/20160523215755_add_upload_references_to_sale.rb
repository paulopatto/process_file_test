class AddUploadReferencesToSale < ActiveRecord::Migration
  def change
    add_column :sales, :upload_id, :integer, index: true
  end
end
