require 'rails_helper'

describe UploadController, type: :controller do
  subject { described_class }

  describe "GET :index" do
    before { get :index }

    it 'returns http success' do
      expect(response).to be_success
    end
  end
end
