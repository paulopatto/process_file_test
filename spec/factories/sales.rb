FactoryGirl.define do
  factory :sale do
    customer "MyString"
    description "MyString"
    price 1.5
    units 1
    address "MyString"
    provider "MyString"
  end
end
